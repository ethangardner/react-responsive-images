import React, { Component } from 'react';
import ResponsiveImg from './ResponsiveImg';
import Picture from './Picture';
import LazyLoad from 'react-lazyload';
import './ItemList.css';

class ItemList extends Component {
  render() {
    let items = this.props.data.map((item, index) => {

      let ctype = '';

      if(this.props.type === 'srcset') {
        ctype = (<ResponsiveImg data={item} />);
      }
      else if(this.props.type === 'picture') {
        ctype = (
          <LazyLoad height={200}>
            <Picture data={item} />
          </LazyLoad>
        );
      }

      return (
        <div key={index} className="grid-50-50">
          {ctype}
        </div>
      );
    });

    return (
      <div className="items">
        {items}
      </div>
    );
  }
}

export default ItemList;
