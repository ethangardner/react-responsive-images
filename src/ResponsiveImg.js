import React, { Component } from 'react';
import './ResponsiveImg.css';

class ResponsiveImg extends Component {
  render() {
    let fallback = this.props.data.filter((item) => item.name === 'l2');
    let sources = this.props.data.map(item => item.url + ' ' + item.w + 'w');

    let sizes = this.props.data.map((item, index, arr) => {
      if (index !== arr.length - 1) {
        return `(max-width: ${item.w}px) ${item.w}px`;
      } else {
        return `${item.w}px`;
      }
    });

    return (
      <img srcSet={sources.join(', ')} sizes={sizes.join(', ')} src={fallback[0].url} alt='' />
    );
  }
}

export default ResponsiveImg;