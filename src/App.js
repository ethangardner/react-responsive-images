import React, { Component } from 'react';
import './App.css';
import data from './data';
import ItemList from './ItemList';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      type: 'srcset'
    };
  }

  componentDidMount() {
    this.setState({
      data: data,
      type: 'srcset'
    });

  }

  change = (e) => {
    let value;
    if (e && e.target) {
      value = e.target.value;
    } else {
      value = e;
    }
    this.setState({
      type: value
    });
  }

  render() {
    return (
      <div className="container">
        <header>
          <h1>Responsive Image Demo</h1>
          <div className="toggle">
            <p>Open your console and watch the network tab as you change sizes or the select box below.</p>
            <select id="lang" onChange={this.change} value={this.state.type}>
              <option value="srcset">srcset</option>
              <option value="picture">picture</option>
            </select>
          </div>
        </header>
        <ItemList data={this.state.data} type={this.state.type} />
      </div>
    );
  }
}

export default App;
