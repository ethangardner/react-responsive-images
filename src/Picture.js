import React, { Component } from 'react';
import Source from './Source';

class Picture extends Component {
  render() {
    let copy = this.props.data.slice(0);
    let sorted = copy.sort(function(a,b) {
        return b.w - a.w;
    });

    let fallback = this.props.data.filter((item) => item.name === 'l2');
    let sourceCollection = [];

    sorted.forEach((item) => {
      sourceCollection.push(<Source media={item.w} srcset={item.url} key={item.url} />);
    });

    return (
      <picture key={fallback[0].url}>
        {sourceCollection}
        <img src={fallback[0].url} alt='' />
      </picture>
    );
  }
}

export default Picture;