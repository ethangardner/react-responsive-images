import React, { Component } from 'react';

class Source extends Component {
  render() {
    let mq = `(min-width: ${this.props.media}px)`

    return(
      <source media={mq} srcSet={this.props.srcset} />
    );
  }
}

export default Source;